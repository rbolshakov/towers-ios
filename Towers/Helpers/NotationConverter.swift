//
//  NotationConverter.swift
//  Towers
//
//  Created by Roman Bolshakov on 04.05.2021.
//

import Foundation

extension String {
    public func components(separatedBy separators: [String]) -> [String] {
        var output: [String] = [self]
        for separator in separators {
            output = output.flatMap { $0.components(separatedBy: separator) }
        }
        return output.map { $0.trimmingCharacters(in: .whitespaces)}
    }
}

struct NotationConverter {
    
    //TODO: improve
    func numericRepresentationFromString(_ string: String) -> [(Int, Int)] {
        var array: [(Int, Int)] = []
        guard string.count > 0 else {
            return []
        }
        let fieldStrings = string.components(separatedBy: ["-", "x"])
        for fieldString in fieldStrings {
            let hor = String(fieldString.first!)
            let ver = String(fieldString.last!)
            guard let i = convertHumanNotationToCoordinate(notationSymbol: hor), let j = convertHumanNotationToCoordinate(notationSymbol: ver)
            else {
                return array
            }
            let tuple = (i, j)
            array.append(tuple)
        }
        return array
    }
    
    func convertCoordinatesToHumanNotation(i: Int, j: Int) -> String? {
        guard let horLetter = convertCoordinateToHumanNotation(coordinate: i, toLetter: true),
              let verLetter = convertCoordinateToHumanNotation(coordinate: j, toLetter: false) else {
            return nil
        }
        return horLetter + verLetter
    }
    
    func convertCoordinateToHumanNotation(coordinate: Int, toLetter: Bool) -> String? {
        if toLetter {
            switch coordinate {
            case 0:
                return "a"
            case 1:
                return "b"
            case 2:
                return "c"
            case 3:
                return "d"
            case 4:
                return "e"
            case 5:
                return "f"
            case 6:
                return "g"
            case 7:
                return "h"
            default:
                return nil
            }
        }
        else {
            if coordinate < 0 || coordinate > 7 {
                return nil
            } else {
                return String("\(coordinate+1)")
            }
        }
    }
    
    func convertHumanNotationToCoordinate(notationSymbol: String?) -> Int? {
        guard let string = notationSymbol else { return nil }
        switch string {
        case "a":
            return 0
        case "b":
            return 1
        case "c":
            return 2
        case "d":
            return 3
        case "e":
            return 4
        case "f":
            return 5
        case "g":
            return 6
        case "h":
            return 7
            
        case "1":
            return 0
        case "2":
            return 1
        case "3":
            return 2
        case "4":
            return 3
        case "5":
            return 4
        case "6":
            return 5
        case "7":
            return 6
        case "8":
            return 7
            
        default:
            return nil
        }
    }
}
