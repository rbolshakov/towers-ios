//
//  ImageManager.swift
//  Towers
//
//  Created by Roman Bolshakov on 07.05.2021.
//

import UIKit
struct ImageManager {
    func drawTower(tower: Tower) -> (UIImage?, CGFloat) {
        let count = tower.white + tower.black
    
        let blackChecker = UIImage(named: "blackChecker5")!
        let whiteChecker = UIImage(named: "whiteChecker")!
        let whiteKing = GameSettings.defaultSettings.preserveKings ? UIImage(named: "whiteKing8")! : UIImage(named: "whiteKing")!
        let blackKing = GameSettings.defaultSettings.preserveKings ? UIImage(named: "blackKing8")! : UIImage(named: "blackKing5")!
        
        let verticalOffset: CGFloat = 160
        
        var towerSize: CGSize = blackChecker.size
        towerSize.height = blackChecker.size.height + verticalOffset * CGFloat(count-1)
        
       // print("\(newSize.height)")
        
        UIGraphicsBeginImageContextWithOptions(towerSize, false, 1.0)
        for i in 0..<count {
            var checker: UIImage!
            if tower.whiteOnTop {
                if (i < tower.black) {
                    let bl = tower.blackCheckers[tower.black - i - 1]
                    checker = (bl == -1) ? blackChecker : blackKing
                } else {
                    let wh = tower.whiteCheckers[i-tower.black]
                    checker = (wh == 1) ? whiteChecker : whiteKing
                }
            } else {
                if (i < tower.white) {
                    let wh = tower.whiteCheckers[tower.white - i - 1]
                    checker = (wh == 1) ? whiteChecker : whiteKing
                } else {
                    let bl = tower.blackCheckers[i - tower.white]
                    checker = (bl == -1) ? blackChecker : blackKing
                }
            }
            let rect = CGRect(x:0, y:+verticalOffset * CGFloat(count - i - 1), width:blackChecker.size.width, height:blackChecker.size.height)
            checker.draw(in: rect)
        }

        let towerImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        let ratio = towerSize.height / blackChecker.size.width
        return (towerImage, ratio)
    }
}
