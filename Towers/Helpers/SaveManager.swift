//
//  SaveManager.swift
//  Towers
//
//  Created by Roman Bolshakov on 06.05.2021.
//

import Foundation
struct SaveManager {    
    func saveToFile(gameRecord: GameRecord, completion: (Bool) -> Void) {
        //var date = GameSettings.defaultSettings.date
        let df = DateFormatter()
        df.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateString = df.string(from: gameRecord.date!)
        let dateString2 = df.string(from: Date())
        let file = "game_\(dateString2).pdn"
        let version = "[Version \"1.3\"]\n"
        let white = "[White \"\(gameRecord.whitePlayerName!)\"]\n"
        let black = "[Black \"\(gameRecord.blackPlayerName!)\"]\n"
        let rules = gameRecord.kingsPreserved ? "[Rules \"Kings preserved\"]\n" : "[Rules \"Kings not preserved\"]\n"
        let towers = "[GameType \"towers\"]\n" //For pdn compatibility
        let result = "[Result \"\(gameRecord.result.textualDescription)\"]\n"
        let dateStr = "[Date \"\(dateString)\"]\n"
        let movesStr = Move.getStringRepresentation(for: gameRecord.moves)
        var text = version + white + black + rules + dateStr + towers + result
        if let position = gameRecord.position {
            let positionString = position.stringRepresentation
            let positionStr = "[FEN \"\(positionString)\"]\n"
            text+=positionStr
        }
        text += movesStr
        text += gameRecord.result.textualDescription
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            print("Saving dir: \(dir)")
            let fileURL = dir.appendingPathComponent(file)
            do {
                try text.write(to: fileURL, atomically: false, encoding: .utf8)
                completion(true)
            }
            catch {
                completion(false)
            }
        }
    }
    
    func openFileWithUrl(fileURL: URL) -> GameRecord? {
        do {
            let text = try String(contentsOf: fileURL, encoding: .utf8)
            let moveStrings = text.components(separatedBy: "\n")
            var white: String?
            var black: String?
            var date: Date?
            var kingsPreserved: Bool = true
            var position: Position?
            var result = Result.Unknown
            
            //TODO: make better
            var moveString: String = ""
            
            let filename = fileURL.lastPathComponent
            let dateString = filename.components(separatedBy: "_")[1]
            print("Date: \(dateString)")
            let df = DateFormatter()
            df.dateFormat = "yyyy-MM-dd HH:mm:ss"
            if let date = df.date(from: dateString) {
                //GameSettings.defaultSettings.date = date
            }
            
            for string in moveStrings {
                if string.contains("FEN") {  //BE CAREFUL! Also contains White/Black so check if first
                    position = nil
                    let positionString = string.components(separatedBy: "\"")[1]
                    position = Position(stringRepresentation: positionString)
                    print("Position: \(position)")
                }
                else if string.contains("White") {
                    white = string.components(separatedBy: "\"")[1]
                }
                else if string.contains("Black") {
                    black = string.components(separatedBy: "\"")[1]
                }
                else if string.contains("Rules") {
                    if string.contains("not preserved") {
                        kingsPreserved = false
                    } else if string.contains("preserved") {
                        kingsPreserved = true
                    }
                }
                else if string.contains("Version") {
                    let version = string.components(separatedBy: "\"")[1]
                    print("Version: \(version)")
                }
                else if string.contains("Result") {
                    let resultString = string.components(separatedBy: "\"")[1]
                    print("Result: \(resultString)")
                    result = Result(string: resultString)
                }
                else if string.contains("Date") {
                    let dateString = string.components(separatedBy: "\"")[1]
                    print("Date: \(dateString)")
                    let df = DateFormatter()
                    df.dateFormat = "yyyy-MM-dd HH:mm:ss"
                    date = df.date(from: dateString)!
                    //GameSettings.defaultSettings.date = date
                }
                else if string.contains("[") {
                    print ("Unknown metadata")
                } else {
                    moveString = moveString + string+"\n"
                }
            }

            let moves = MoveGenerator().getMoveListFromString(string: moveString, position: position ?? Position(.initial))
            let gameRecord = GameRecord(position: position, moves: moves, whitePlayerName: white, blackPlayerName: black, kingsPreserved: kingsPreserved, date: date, result: result)
            return gameRecord
        }
        catch {/* error handling here */}
        return nil
    }
    
    func getDocuments() -> [URL]{
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            do {
                let fileURLs = try FileManager.default.contentsOfDirectory(at: dir, includingPropertiesForKeys: nil)
                var filtered: [URL] = []
                for url in fileURLs {
                    if !url.lastPathComponent.hasPrefix(".") {
                        filtered+=[url]
                    }
                }
                return filtered.sorted { (url1, url2) -> Bool in
                    return url1.absoluteString > url2.absoluteString
                }
                // process files
            } catch {
                return []
                //print("Error while enumerating files \(documentsURL.path): \(error.localizedDescription)")
            }
        }
        return []
    }
}
