//
//  SetupBoardController.swift
//  Towers
//
//  Created by Roman Bolshakov on 13.05.2021.
//
import UIKit

class SetupBoardController: UIViewController {
    var checkerType: CheckerType = .empty
    enum CheckerType: Int {
        case whiteChecker = 0, whiteKing, blackChecker, blackKing, empty
    }
    
    @IBOutlet weak var graphicBoard: UIStackView!
    @IBOutlet weak var bottomIndicator: UIView?
    @IBOutlet weak var topIndicator: UIView?
    
    @IBOutlet weak var whiteCheckerButton: UIButton!
    @IBOutlet weak var whiteKingButton: UIButton!
    @IBOutlet weak var blackCheckerButton: UIButton!
    @IBOutlet weak var blackKingButton: UIButton!
    @IBOutlet weak var emptyCheckerButton: UIButton!
    @IBOutlet weak var turnSwitch: UISwitch!
    @IBOutlet weak var invertedSwitch: UISwitch!
    
    var invertedBoard = false
    var position: Position = Position(.initial)
    var copied: Position = Position(.initial)
    
    var didChangePosition: ((Position) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        invertedSwitch.isOn = invertedBoard
        copied = position
        drawBoard()
    }
    
    @IBAction func initialPositonAction(_ sender: Any) {
        position = Position(.initial)
        drawBoard()
    }
    
    @IBAction func emptyPositonAction(_ sender: Any) {
        position = Position(.empty)
        drawBoard()
    }
    
    @IBAction func resetPositonAction(_ sender: Any) {
        position = copied
        drawBoard()
    }
    
    @IBAction func changeTurn(_ sender: Any) {
        position.whiteTurn = turnSwitch.isOn
        //drawBoard() - not needed
    }
    
    @IBAction func invertBoard(_ sender: Any) {
        invertedBoard = invertedSwitch.isOn
        drawBoard()
    }
    
    
    func disableAllHighlights() {
        whiteCheckerButton.backgroundColor = .clear
        whiteKingButton.backgroundColor = .clear
        blackCheckerButton.backgroundColor = .clear
        blackKingButton.backgroundColor = .clear
        emptyCheckerButton.backgroundColor = .clear
    }
    
    @IBAction func buttonSelected(_ sender: Any) {
        disableAllHighlights()
        let button = sender as! UIButton
        button.backgroundColor = .red
        checkerType = CheckerType(rawValue: button.tag)!
    }
    
    @IBAction func fieldPressed(_ sender: Any) {
        let tag = (sender as! UIButton).tag
        let i = tag/8
        var j = 7 - tag%8
        print("\(checkerType) on field \(i) \(j)")
        
        //Also use field above
        if i%2 != j%2 {
            if invertedBoard && j < 7 {
                j += 1
            }
            else if !invertedBoard && j > 0 {
                j-=1
            }
            else {
                return
            }
        }
        if let tower = position[i, j] {
            var newTower: Tower! = nil
            var checkers = tower.checkers
            switch checkerType {
            case .empty:
                checkers=Array(checkers[1..<checkers.count])
                newTower = Tower(checkers: checkers)
            case .whiteChecker:
                checkers=[1]+checkers
                newTower = Tower(checkers: checkers)
                if newTower == nil { return }
            case .whiteKing:
                checkers=[2]+checkers
                newTower = Tower(checkers: checkers)
                if newTower == nil { return }
            case .blackChecker:
                checkers=[-1]+checkers
                newTower = Tower(checkers: checkers)
                if newTower == nil { return }
            case .blackKing:
                checkers=[-2]+checkers
                newTower = Tower(checkers: checkers)
                if newTower == nil { return }
            }
            position[i, j] = newTower
        } else {
            var tower: Tower! = nil
            switch checkerType {
            case .empty:
                break
            case .whiteChecker:
                tower = Tower(.whiteChecker)
            case .whiteKing:
                tower = Tower(.whiteKing)
            case .blackChecker:
                tower = Tower(.blackChecker)
            case .blackKing:
                tower = Tower(.blackKing)
            }
            position[i,j] = tower
        }
        drawBoard()
    }
    
    func drawBoard() {
        bottomIndicator?.isHidden = position.whiteTurn != invertedBoard
        topIndicator?.isHidden = position.whiteTurn != invertedBoard
        bottomIndicator?.isHidden = true
        topIndicator?.isHidden = true
        bottomIndicator?.backgroundColor = invertedBoard ? .black : .white
        topIndicator?.backgroundColor = invertedBoard ? .white : .black
        //scoreLabel.text = "\(position.score)"
        turnSwitch.isOn = position.whiteTurn

        
        for (columnIndex, column) in graphicBoard.arrangedSubviews.enumerated() {
            for (rowIndex, towerViewplace) in (column as! UIStackView).arrangedSubviews.enumerated() {
                let towerView : TowerView = Bundle.main.loadNibNamed("TowerView", owner: self, options: nil)![0] as! TowerView
                if invertedBoard {
                    towerView.setupWithTower(tower: position[7-columnIndex, rowIndex])
                } else {
                    towerView.setupWithTower(tower: position[columnIndex, 7 - rowIndex])
                }
                towerView.frame = towerViewplace.frame
                if invertedBoard {
                    towerView.button.tag = (7-columnIndex) * 8 + 7 - rowIndex
                } else {
                    towerView.button.tag = columnIndex * 8 + rowIndex
                }
                let columnStackView = (column as! UIStackView)
                towerViewplace.removeFromSuperview()
                columnStackView.insertArrangedSubview(towerView, at: rowIndex)
            }
        }
    }
    
    @IBAction func saveSettings (_ sender: Any) {
        didChangePosition?(position)
    }
    
    @IBAction func dismissScreen(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
