//
//  AIModuleForced.swift
//  Towers
//
//  Created by Roman Bolshakov on 25.05.2021.
//

import Foundation

class AIModuleForced: AIModule {
    let evaluation: EvaluationFunction
    
    required init(evaluation: EvaluationFunction) {
        self.evaluation = evaluation
    }
    
    let moveGenerator = MoveGenerator()
    let moveMaker = MoveMaker()
    var total = 0
    
    func findBestMove(position: Position, strength: AIPlayer.Strength) -> (Move?, Int?) {
        var result: (Move?, Int?, Int?)
        let startTime = Date()
        var interval: TimeInterval = TimeInterval(0)
        var depth = 1
        total = 0
        while interval < strength.timeInterval {
            result = findBestMove(position: position, depth: depth, startDepth: depth, alpha:-99)
            let endTime = Date()
            interval = endTime.timeIntervalSince(startTime)
            if let movesCount = result.2, movesCount == 1 { //Only move
                break
            }
            depth+=1
        }
        print ("Reached depth \(depth) with full interval \(interval)")
        return (result.0!, result.1)
    }
    
    private func findBestMove(position: Position, depth: Int, startDepth: Int, alpha: Int) -> (Move?, Int?, Int?) {
        let moves = moveGenerator.getAllMoves(position: position)
        
        guard !moves.isEmpty else {
            //total+=1
            return (nil, -100, 0)
        }
    
        guard depth > -startDepth else { //Stop calculating even beats. Also we don't know anything about the position score at this point
            //total+=1
            return (nil, nil, moves.count)
        }
        
        let needBeat = moves[0].simpleMoves[0].beatField != nil
        
        guard depth > 0 || needBeat else { //Skip calculating non-beats
            //total+=1
            
            if depth < 0 {
                total+=1
            } else {
                total+=1
            }
            
            if position.whiteTurn {
                return (nil, evaluation.score(for: position), moves.count)
            } else {
                return (nil, -evaluation.score(for: position), moves.count)
            }
        }

        var max = -100
        var best: Int?
        
        for (index, move) in moves.enumerated() {
            let newPosition = moveMaker.makeMove(position: position, move: move)
            let (_, returnedScore, _) = findBestMove(position: newPosition, depth: depth - 1, startDepth: startDepth, alpha: max)
            
            guard let returned = returnedScore else {
                continue //IGNORE THIS SHIT, BECAUSE THAT MUST BE SOME STUPID LINE ANYWAY, WHICH WILL NEVER BE PLAYED IN REAL GAME
            }
            let score = -returned
            
            if score > max {
                max = score
                best = index
            }
            
            if score > -alpha {
                break
            }
        }
        
        guard let bestFound = best else {
            if depth == startDepth {
                return (moves.randomElement(), nil, moves.count)
            } else {
                return (nil, nil, moves.count)
            }
        }
        return (moves[bestFound], max, moves.count)
    }
}
