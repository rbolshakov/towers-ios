//
//  AiModuleNonForced.swift
//  Towers
//
//  Created by Roman Bolshakov on 25.05.2021.
//

import Foundation
//
//  Game.swift
//  Towers
//
//  Created by Roman Bolshakov on 07.05.2021.
//

import Foundation
class AIModuleNonForced: AIModuleForced {
    
    override func findBestMove(position: Position, strength: AIPlayer.Strength) -> (Move?, Int?) {
        if let (move, score) = findBestMove(position: position, strength: strength) {
            return (move, score)
        } else {
            return (nil, nil)
        }
    }
    
    func findBestMove(position: Position, strength: AIPlayer.Strength) -> (Move, Int)? {
        var result: (Move, Int, Int)?
        let startTime = Date()
        var interval: TimeInterval = TimeInterval(0)
        var depth = 1
        total = 0
        while interval < strength.timeInterval {
            result = findBestMove(position: position, depth: depth, alpha:-99)
            let endTime = Date()
            interval = endTime.timeIntervalSince(startTime)
            if let result = result, result.2 == 1 { //Only move
                break
            }
            depth+=1
        }
        print ("Reached depth \(depth) with full interval \(interval)")
        if let result = result {
            return (result.0, result.1)
        } else {
            return nil
        }
    }
    
    private func findBestMove(position: Position, depth: Int, alpha: Int) -> (Move, Int, Int)? {
        let moves = moveGenerator.getAllMoves(position: position)
        if moves.isEmpty { return nil }
        
        var max = -100
        var best = 0
        
        for (index, move) in moves.enumerated() {
            let newPosition = moveMaker.makeMove(position: position, move: move)
            var score: Int
            if depth == 0 {
                if position.whiteTurn {
                    score = evaluation.score(for: newPosition)
                } else {
                    score = -evaluation.score(for: newPosition)
                }
            }
            else if let (_, returnedScore, _) = findBestMove(position: newPosition, depth: depth - 1, alpha: max) {
                score = -returnedScore
            } else { //NO MOVES FOR OPPONENT = BEST MOVE FOR US
                score = 100
            }
            
            total+=1
            
            if score > max {
                max = score
                best = index
            }
            
            if score > -alpha {
                break
            }
        }
        return (moves[best], max, moves.count)
    }
}
