//
//  Checkerboard.swift
//  Towers
//
//  Created by Roman Bolshakov on 03.05.2021.
//

import Foundation

struct Position : Equatable {
    
    static func == (lhs: Position, rhs: Position) -> Bool {
        if lhs.whiteTurn != rhs.whiteTurn { return false }
        if lhs.checkerboard != rhs.checkerboard { return false }
        return true
    }
    
    var checkerboard: [[Tower?]] = Array(repeating: Array(repeating: nil, count: 8), count: 8)
    var whiteTurn: Bool = true
    let converter = NotationConverter()
    
    enum PositionType {
        case empty, initial
    }
    
    init(_ type: PositionType) {
        switch type {
        case .empty:
            clearCheckerboard()
        default:
            setInitialPosition()
        }
    }
    
    init() {
        self.init(.initial)
    }
    
    init?(stringRepresentation: String) {
        let towerStrings = stringRepresentation.components(separatedBy: " ")
        for towerString in towerStrings {
            if towerString == "White" {
                whiteTurn = true
                continue
            } else if towerString == "Black" {
                whiteTurn = false
                continue
            }
            let components = towerString.components(separatedBy: "#")
            guard components.count == 2 else { return nil }
            let coordinate = components[0] as String
            let towerStringRepresentation = components[1]
            let tower = Tower(stringRepresentation: towerStringRepresentation)
            self[coordinate] = tower
        }
    }
    
    static func isValidField (_ i: Int, _ j: Int) -> Bool {
        return 0...7~=i && 0...7~=j
    }
    
    static func KingHor (isWhite: Bool) -> Int {
        return isWhite ? 7 : 0
    }
    
    subscript(indexH: Int, indexV: Int) -> Tower? {
        get {
            checkerboard[indexH][indexV]
        }
        set {
            checkerboard[indexH][indexV] = newValue
        }
    }
    
    subscript(index: String) -> Tower? {
        
        get {
            guard index.count == 2 else { return nil }
            let hor = index.first
            let ver = index.last
            
            guard let i = converter.convertHumanNotationToCoordinate(notationSymbol: String(hor!)),
                  let j = converter.convertHumanNotationToCoordinate(notationSymbol: String(ver!)) else {
                    return nil
                    
                  }
            return checkerboard[i][j]
        }
        set {
            guard index.count == 2 else { return }
            let hor = index.first
            let ver = index.last
            
            guard let i = converter.convertHumanNotationToCoordinate(notationSymbol: String(hor!)),
                  let j = converter.convertHumanNotationToCoordinate(notationSymbol: String(ver!)) else {
                    return
                    
                  }
            checkerboard[i][j] = newValue
        }
        
    }
    
    mutating func clearCheckerboard() {
        for i in 0..<8 {
            for j in 0..<8 {
                checkerboard[i][j] = nil
            }
        }
    }
    
    mutating func setInitialPosition() {
        for i in 0..<8 {
            for j in 0..<8 {
                if 0...2 ~= j {
                    if (i%2 == j%2) {
                        checkerboard[i][j] = Tower(.whiteChecker)
                    } else {
                        checkerboard[i][j] = nil
                    }
                }
                else if 5...7 ~= j {
                    if (i%2 == j%2) {
                        checkerboard[i][j] = Tower(.blackChecker)
                    } else {
                        checkerboard[i][j] = nil
                    }
                    
                }
                else {
                    checkerboard[i][j] = nil
                }
            }
        }
    }
    
    static private var custom1: Position {
        var position = Position(.empty)
        position["g1"] = Tower(.whiteTower(15, king: true))
        position["c1"] = Tower(.blackTower(5))
        position["b2"] = Tower(.blackTower(5, king: true))
        position["a5"] = Tower(.blackKing)
        position["h4"] = Tower(.blackChecker)
        position["h6"] = Tower(.blackChecker)
        return position
    }
    
    static private var custom2: Position {
        var position = Position(.empty)
        position["h6"] = Tower(.blackChecker)
        position["f4"] = Tower(.blackChecker)
        position["f8"] = Tower(.blackChecker)
        position["g7"] = Tower(white: 10, black: 1, whiteOnTop: false, isKing: true)
        position["h4"] = Tower(.whiteTower(2))
        return position
    }
    
    static private var custom3: Position {
        var position = Position(.empty)
        position["c3"] = Tower(.whiteKing)
        position["d4"] = Tower(.blackKing)
        position["b8"] = Tower(.blackKing)
        
        var tower = Tower(white: 4, black: 4, whiteOnTop: true, isKing: true)
        tower?.whiteCheckers = [1,1,1,2]
        tower?.blackCheckers = [-1,-1,-1,-2]
        position["a1"] = tower
        position["c1"] = Tower(checkers: [-2,-1,1,2,1,2])
        return position
    }
    
    static var custom: [Position] {
        return [custom1, custom2, custom3]
    }
    
    var stringRepresentation: String {
        let converter = NotationConverter()
        var text = whiteTurn ? "White" : "Black"
        for i in 0..<8 {
            for j in 0..<8 {
                if let tower = checkerboard[i][j] {
                    let coordinateText = converter.convertCoordinatesToHumanNotation(i: i, j: j)!
                    let towerString = tower.stringRepresentation
                    text+=" \(coordinateText)#\(towerString)"
                }
            }
        }
        return text
    }
}
