//
//  GameRecord.swift
//  Towers
//
//  Created by Roman Bolshakov on 20.05.2021.
//

import Foundation

enum Result {
    case WhiteWin, BlackWin, Draw, Unknown
    var textualDescription: String {
        switch self {
        case .WhiteWin:
            return "1-0"
        case .BlackWin:
            return "0-1"
        case .Draw:
            return "1/2-1/2"
        case .Unknown:
            return "*"
        }
    }
    
    var message: String {
        switch self {
        case .WhiteWin:
            return "White won"
        case .BlackWin:
            return "Black won"
        case .Draw:
            return "Draw by repetition"
        case .Unknown:
            return ""
        }
    }
    
    init (string: String) {
        switch string {
        case "1-0":
            self = .WhiteWin
        case "0-1":
            self = .BlackWin
        case "1/2-1/2":
            self = .Draw
        default:
            self = .Unknown
        }
    }
}

struct GameRecord {
    var position: Position?
    var moves: [Move]
    var whitePlayerName: String?
    var blackPlayerName: String?
    var kingsPreserved: Bool = true
    var date: Date?
    var result: Result
    
    var textualDescription: String {
        var string = ""
        if let date = date {
            let df = DateFormatter()
            df.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let dateString = df.string(from: date)
            string+="Date: \(dateString)\n"
        }
        let rules = kingsPreserved ? "Rules: kings preserved\n" : "Rules: kings not preserved\n"
        string+=rules
        if let position = position, position != Position(.initial) {
            string+="Position: custom\n"
        } else {
            string+="Position: initial\n"
        }
        if let white = whitePlayerName {
            string+="White: \(white)\n"
        }
        if let black = blackPlayerName {
            string+="Black: \(black)\n"
        }
        let moveCount: Int = moves.count % 2 == 0 ? moves.count / 2: moves.count/2 + 1
        string+="Moves: \(moveCount)\n"
        string+="Result: \(result.textualDescription)"
        return string
    }
}
