//
//  MoveMaker.swift
//  Towers
//
//  Created by Roman Bolshakov on 07.05.2021.
//

import Foundation
struct MoveMaker {
    
    func makeSimpleMove(position: Position, move: SimpleMove) -> Position {
        var returnPosition = position
        guard var tower = position.checkerboard[move.startField.i][move.startField.j] else {
            assertionFailure()
            return position
        }
        if move.becameKing {
            tower.setKing(isKing: true)
        }
        if let beatField = move.beatField {
            guard var enemyTower = position.checkerboard[beatField.i][beatField.j] else {
                assertionFailure()
                return position
            }
            let isKing = enemyTower.isKing
            let valid = enemyTower.removeTopChecker()
            returnPosition.checkerboard[beatField.i][beatField.j] = valid ? enemyTower : nil
            tower.eatEnemyChecker(isKing: isKing)
        }
        returnPosition.checkerboard[move.startField.i][move.startField.j] = nil
        returnPosition.checkerboard[move.endField.i][move.endField.j] = tower
        return returnPosition
    }
    
    func undoSimpleMove(position: Position, move: SimpleMove) -> Position {
        var returnPosition = position
        guard var tower = position.checkerboard[move.endField.i][move.endField.j] else {
            assertionFailure()
            return position
        }
        if let beatField = move.beatField {
            _ = tower.removeLastEatenChecker()
            var enemyTower: Tower
            if var existingTower = position.checkerboard[beatField.i][beatField.j] {
                existingTower.addTopChecker(white: position.whiteTurn, isKing: move.beatKing)
                enemyTower = existingTower
            } else {
                enemyTower = Tower(position.whiteTurn ? .whiteChecker : .blackChecker)
                enemyTower.setKing(isKing: move.beatKing)
            }
            returnPosition.checkerboard[beatField.i][beatField.j] = enemyTower
        }
        
        if move.becameKing {
            tower.setKing(isKing: false)
        }
        
        returnPosition.checkerboard[move.endField.i][move.endField.j] = nil
        returnPosition.checkerboard[move.startField.i][move.startField.j] = tower
        return returnPosition
    }
    
    func makeMove(position: Position, move: Move) -> Position {
        var returnPosition = position
        for simpleMove in move.simpleMoves {
            returnPosition = makeSimpleMove(position: returnPosition, move: simpleMove)
        }
        returnPosition.whiteTurn = !returnPosition.whiteTurn
        return returnPosition
    }
    
    func undoMove(position: Position, move: Move) -> Position {
        var returnPosition = position
        for simpleMove in move.simpleMoves.reversed() {
            returnPosition = undoSimpleMove(position: returnPosition, move: simpleMove)
        }
        returnPosition.whiteTurn = !returnPosition.whiteTurn
        return returnPosition
    }
}
