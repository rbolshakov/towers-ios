//
//  SimpleMove.swift
//  Towers
//
//  Created by Roman Bolshakov on 04.05.2021.
//

import Foundation
struct SimpleMove {
    let startField: (i: Int, j: Int)
    let endField: (i: Int, j: Int)
    let becameKing: Bool
    let beatField: (i: Int, j: Int)?
    let beatKing: Bool
}
