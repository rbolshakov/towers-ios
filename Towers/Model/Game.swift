//
//  Game.swift
//  Towers
//
//  Created by Roman Bolshakov on 07.05.2021.
//

import Foundation

protocol GameDelegate: AnyObject {
    func gameMadeMove (move: Move)
    func gameUndoMove (move: Move)
    func gameUpdatedPosition ()
    func gameChangedTurn (player: Player)
    func computerGotScore (score: Int?)
    func gameFinished (result: Result)
}

class Game {
    private(set) var position: Position = Position(.initial)
    var savedPosition: Position?
    var moves: [Move] = []
    var moveMaker = MoveMaker()
    var moveGenerator = MoveGenerator()
    var delegate: GameDelegate?
    var players: [Player] = []
    var currentPlayer: Player! = nil
    var currentMoveIndex = 0
    var gameResult = Result.Unknown
    var date = Date()
    
    init() {
    }
    
    func newGame (players: [Player], position: Position) {
        self.position = position
        self.savedPosition = position
        delegate?.gameUpdatedPosition()
        self.players = players
        moves = []
        currentMoveIndex = 0
        gameResult = .Unknown
        date = Date()
        startGame()
    }
    
    func startGame() {
        currentPlayer = players.first
        delegate?.gameChangedTurn(player: currentPlayer)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0) {
            self.currentPlayer.askForNextMove(position: self.position)
        }
    }
    
    func saveGame(completion: (Bool) -> Void) {
        let whitePlayer = players[0]
        let blackPlayer = players[1]
        var positionToSave: Position?
        if savedPosition != Position(.initial) {
            positionToSave = savedPosition
        }
        
        var result: Result = .Unknown
        if gameResult == .Unknown {
            result = checkGameResult() //Could use gameResult directly but this allows to overwrite old saves with no result
        } else {
            result = gameResult
        }

        let gameRecord = GameRecord(position: positionToSave, moves: moves, whitePlayerName: whitePlayer.name, blackPlayerName: blackPlayer.name, kingsPreserved: GameSettings.defaultSettings.preserveKings, date: date, result: result)
        SaveManager().saveToFile(gameRecord: gameRecord) { (success) in
            completion(success)
        }
    }
    
    func undoLastMove() {
        guard currentMoveIndex - 1 < moves.count, currentMoveIndex > 0 else {
            return;
        }
        let moveToUndo = moves[currentMoveIndex-1]
        position = moveMaker.undoMove(position: position, move: moveToUndo)
        currentMoveIndex-=1
        gameResult = .Unknown
        delegate?.gameUndoMove(move: moveToUndo)
        switchPlayers()
        delegate?.gameChangedTurn(player: currentPlayer)
    }
    
    func redoLastMove() {
        guard currentMoveIndex < moves.count else { return }
        let moveToRedo = moves[currentMoveIndex]
        position = moveMaker.makeMove(position: position, move: moveToRedo)
        currentMoveIndex+=1
        delegate?.gameMadeMove(move: moveToRedo)
        switchPlayers()
        delegate?.gameChangedTurn(player: currentPlayer)
    }
    
    func switchToMove (moveIndex: Int) {
        guard moveIndex < moves.count else { return }
        position = savedPosition ?? Position(.initial)
        currentMoveIndex = 0
        while currentMoveIndex <= moveIndex {
            let move = moves[currentMoveIndex]
            position = moveMaker.makeMove(position: position, move: move)
            currentMoveIndex+=1
        }
        gameResult = .Unknown
        self.delegate?.gameUpdatedPosition()
    }
    
    func loadGame(record: GameRecord) {
        GameSettings.defaultSettings.preserveKings = record.kingsPreserved
        GameSettings.defaultSettings.computerPlaysWhite = false
        GameSettings.defaultSettings.computerPlaysBlack = false
        let whitePlayer = HumanPlayer(game: self)
        if let name = record.whitePlayerName {
            whitePlayer.name = name
        }
        let blackPlayer = HumanPlayer(game: self)
        if let name = record.blackPlayerName {
            blackPlayer.name = name
        }
        players = [whitePlayer, blackPlayer]
        self.position = record.position ?? Position(.initial)
        self.savedPosition = self.position
        replayMoves(moves: record.moves)
        self.gameResult = record.result
        if let date = record.date {
            self.date = date
        } else {
            self.date = Date()
        }
        self.currentPlayer = self.position.whiteTurn ? players[0] : players[1]
    }
    
    func replayMoves(moves: [Move]) {
        self.moves = moves
        currentMoveIndex = 0
        for move in moves {
            position = moveMaker.makeMove(position: position, move: move)
            currentMoveIndex+=1
        }
        self.delegate?.gameUpdatedPosition()
    }
    
    func checkLineIsValidMove (line: [(Int, Int)]) -> (valid: Bool, exact: Move?) {
        return moveGenerator.checkLineIsValidMove(line: line, for: position)
    }
    
    
    func switchPlayers() {
        for player in self.players {
            if player !== self.currentPlayer {
                self.currentPlayer = player
                break
            }
        }
        
        delegate?.gameChangedTurn(player: currentPlayer)
    }
    
    func makeMove(move: Move) {
        if gameResult != .Unknown {
            return
        }
        position = moveMaker.makeMove(position: position, move: move)
        if currentMoveIndex>=moves.count {
            moves+=[move]
        } else {
            moves[currentMoveIndex] = move
            moves.removeSubrange(currentMoveIndex+1..<moves.count)
        }
        currentMoveIndex+=1
        delegate?.gameMadeMove(move: move)
        gameResult = self.checkGameResult()
        if gameResult != .Unknown { return }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.switchPlayers()
            self.currentPlayer.askForNextMove(position: self.position)
        }
    }
    
    func forceMove() {
        switchPlayers()
        self.currentPlayer.askForNextMove(position: self.position)
    }
    
    func sendScore (score: Int?) {
        delegate?.computerGotScore(score: score)
    }
    
    func checkGameResult() -> Result {
        var result: Result = .Unknown
        if moveGenerator.getAllMoves(position: position).count == 0 {
            result = position.whiteTurn ? .BlackWin : .WhiteWin
            delegate?.gameFinished(result: result)
        } else if checkForDraw() == true {
            result = .Draw
            delegate?.gameFinished(result: result)
        }
        return result
    }
    
    func getMoveListFromString (string: String) -> [Move] {
        var moves: [Move] = []
        let moveStrings = string.components(separatedBy: "\n")
        let generator = MoveGenerator()
        let moveMaker = MoveMaker()
        let converter = NotationConverter()
        var position: Position = self.position
        for moveString in moveStrings {
            var convertedString: String!
            if moveString.contains(".") {
                convertedString = moveString.components(separatedBy: ". ")[1]
            } else {
                convertedString = moveString
            }
            //TODO: failable
            let numeric = converter.numericRepresentationFromString(convertedString)
            let (_, exact) = generator.checkLineIsValidMove(line: numeric, for: position)
            if let move = exact {
                position = moveMaker.makeMove(position: position, move: move)
                moves.append(move)
            }
        }
        return moves
    }
    
    
    func checkForDraw() -> Bool {
        let positions = allPositionsFromMoveList(moves: moves, from: savedPosition!)
        return checkTripleRepetition(positions: positions)
    }
    
    //TODO: add custom position
    func allPositionsFromMoveList (moves: [Move], from position: Position) -> [Position] {
        var positions: [Position] = []
        var position = position
        let moveMaker = MoveMaker()
        for move in moves {
            position = moveMaker.makeMove(position: position, move: move)
            positions.append(position)
        }
        return positions
    }
    
    func checkTripleRepetition (positions: [Position]) -> Bool {
        for position in positions {
            var matches = 0
            for matchedPosition in positions {
                if matchedPosition == position {
                    matches+=1
                    if matches == 3 {
                        return true
                    }
                }
            }
        }
        return false
    }
}
