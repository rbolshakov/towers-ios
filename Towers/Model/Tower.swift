//
//  Tower.swift
//  Towers
//
//  Created by Roman Bolshakov on 03.05.2021.
//

import Foundation
struct Tower : Equatable {
    var whiteOnTop: Bool
    var whiteCheckers: [Int] = []
    var blackCheckers: [Int] = []
    
    
    var white: Int {
        get {
            whiteCheckers.count
        }
    }
    var black: Int {
        get {
            blackCheckers.count
        }
    }
    
    init?(checkers: [Int]) {
        guard checkers.count > 0 else { return nil }
        whiteOnTop = checkers.first! > 0
        var topHalf: [Int] = []
        var bottomHalf: [Int] = []
        var isTop = true
        for checker in checkers {
            guard checker == 1 || checker == 2 || checker == -1 || checker == -2 else { return nil}
            if isTop {
                if (whiteOnTop && checker < 0 ) || (!whiteOnTop && checker > 0) {
                    isTop = false
                    bottomHalf.append(checker)
                } else {
                    topHalf.append(checker)
                }
            } else {
                bottomHalf.append(checker)
                if (whiteOnTop && checker > 0) || (!whiteOnTop && checker < 0) {
                    return nil
                }
            }
        }
        if whiteOnTop {
            whiteCheckers = topHalf.reversed()
            blackCheckers = bottomHalf
        } else {
            blackCheckers = topHalf.reversed()
            whiteCheckers = bottomHalf
        }
        
    }
    
    init?(stringRepresentation: String) {
        var checkers: [Int] = []
        for character in stringRepresentation {
            switch character {
            case "w":
                checkers.append(1)
            case "W":
                checkers.append(2)
            case "b":
                checkers.append(-1)
            case "B":
                checkers.append(-2)
            default:
                break
            }
        }
        self.init(checkers: checkers)
    }
    
    var checkers: [Int] {
        get {
            var value: [Int] = []
            if whiteOnTop {
                value = whiteCheckers.reversed()
                value+=blackCheckers
            } else {
                value = blackCheckers.reversed()
                value+=whiteCheckers
            }
            return value
        }
    }
    
    var stringRepresentation: String {
        var string = ""
        for checker in checkers {
            switch checker {
            case 1:
                string+="w"
            case 2:
                string+="W"
            case -1:
                string+="b"
            case -2:
                string+="B"
            default:
                break
            }
        }
        return string
    }
    
    var isKing: Bool {
        get {
            if whiteOnTop {
                return whiteCheckers.last == 2
            } else {
                return blackCheckers.last == -2
            }
        }
    }
    
    mutating func setKing (isKing: Bool) {
        let value = isKing ? 2 : 1
        if whiteOnTop {
            whiteCheckers[whiteCheckers.count - 1] = value
        } else {
            blackCheckers[blackCheckers.count - 1] = -value
        }
    }
    
    enum Tower {
        case whiteChecker, whiteKing, blackChecker, blackKing
        case whiteTower(Int, king: Bool = false)
        case blackTower(Int, king: Bool = false)
    }
    
    init?(white: Int, black: Int, whiteOnTop: Bool, isKing: Bool) {
        if white < 0 || black < 0 || white+black == 0 || (white == 0 && whiteOnTop) || (black == 0 && !whiteOnTop) {
            return nil
        }
        self.whiteOnTop = whiteOnTop
        whiteCheckers = Array(repeating: 1, count: white)
        blackCheckers = Array(repeating: -1, count: white)
        setKing(isKing: isKing)
    }
    
    init?(white: [Int], black: [Int], whiteOnTop: Bool) {
        if white.count < 0 || black.count < 0 || white.count+black.count == 0 || (white.count == 0 && whiteOnTop) || (black.count == 0 && !whiteOnTop) {
            return nil
        }
        self.whiteOnTop = whiteOnTop
        for whiteChecker in white {
            if whiteChecker != 1 || whiteChecker != 2 {
                return nil
            }
        }
        for blackChecker in black {
            if blackChecker != -1 || blackChecker != -2 {
                return nil
            }
        }
        whiteCheckers = white
        blackCheckers = black
    }
    
    //TODO: add initializer with just one array
    
    init(_ type: Tower) {
        switch type {
        case .whiteChecker:
            whiteOnTop = true
            whiteCheckers = [1]
        case .blackChecker:
            whiteOnTop = false
            blackCheckers = [-1]
        case .whiteKing:
            whiteOnTop = true
            whiteCheckers = [2]
        case .blackKing:
            whiteOnTop = false
            blackCheckers = [-2]
        case .whiteTower (let total, let king):
            whiteOnTop = true
            whiteCheckers = Array(repeating: 1, count: total)
            setKing(isKing: king)
        case .blackTower (let total, let king):
            whiteOnTop = false
            blackCheckers = Array(repeating: -1, count: total)
            setKing(isKing: king)
        }
    }
    
    mutating func addTopChecker(white: Bool, isKing: Bool) {
        whiteOnTop = white
        if whiteOnTop {
            //self.white += 1
            whiteCheckers.append(isKing ? 2 : 1)
        } else {
            //self.black += 1
            blackCheckers.append(isKing ? -2 : -1)
        }
    }
    
    mutating func removeTopChecker() -> (Bool) {
        if white + black == 1 {
            return false
        }
        if whiteOnTop {
            whiteCheckers.removeLast()
            if white == 0 {
                whiteOnTop = false
                blackCheckers = blackCheckers.reversed()
            }
        } else {
            blackCheckers.removeLast()
            if black == 0 {
                whiteOnTop = true
                whiteCheckers = whiteCheckers.reversed()
            }
        }
        return true
    }
    
    mutating func eatEnemyChecker(isKing: Bool = false) {
        if whiteOnTop {
            blackCheckers.append(isKing && GameSettings.defaultSettings.preserveKings ? -2 : -1)
        } else {
            whiteCheckers.append(isKing && GameSettings.defaultSettings.preserveKings ? 2 : 1)
        }
    }
    
    mutating func removeLastEatenChecker() -> (Bool) {
        if whiteOnTop {
            if black > 0 {
                blackCheckers.removeLast()
                return true
            } else {
                return false
            }
        } else {
            if white > 0 {
                whiteCheckers.removeLast()
                return true
            } else {
                return false
            }
        }
    }
}
