//
//  IntegerEvaluation.swift
//  Towers
//
//  Created by Roman Bolshakov on 25.05.2021.
//

import Foundation
struct IntegerEvaluation: EvaluationFunction {
    func score(for position: Position) -> Int {
        var total = 0
        var white = 0
        var black = 0
        for i in 0..<8 {
            for j in 0..<8 {
                if let tower = position[i, j] {
                    if tower.whiteOnTop {
                        white+=1
                        total+=tower.white
                        if tower.isKing {
                            total+=5
                        }
                        else if tower.white < 3 && tower.black >= tower.white * 2 {
                            total-=tower.white
                        }
                        if tower.white > 1 {
                            total+=tower.white/2
                        }
                    } else {
                        black+=1
                        total-=tower.black
                        if tower.isKing {
                            total-=5
                        }
                        else if tower.black < 3 && tower.white >= tower.black * 2 {
                            total+=tower.black
                        }
                        if tower.black > 1 {
                            total-=tower.black/2
                        }
                    }
                }
            }
        }
        if white == 0 {
            return -100
        } else if black == 0 {
            return 100
        } else {
            return total
        }
        //TODO: check does sides have moves?
    }
}
