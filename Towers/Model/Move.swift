//
//  Move.swift
//  Towers
//
//  Created by Roman Bolshakov on 04.05.2021.
//

import Foundation
struct Move {
    var simpleMoves: [SimpleMove] = []
    
    init() {
        
    }
    
    init(_ move: SimpleMove) {
        simpleMoves.append(move)
    }

    init(_ moves: [SimpleMove]) {
        simpleMoves+=moves
    }
    
    mutating func addMove (move: SimpleMove) {
        simpleMoves.append(move)
    }
    
    func getNumericRepresentation() -> [(Int, Int)] {
        var array: [(Int, Int)] = []
        for (index, simpleMove) in simpleMoves.enumerated() {
            let i = simpleMove.startField.i
            let j = simpleMove.startField.j
            array+=[(i,j)]
            if index == simpleMoves.count - 1 {
                let i = simpleMove.endField.i
                let j = simpleMove.endField.j
                array+=[(i,j)]
            }
        }
        return array
    }
    
    func getStringRepresentation(showBeats: Bool = false) -> String {
        var string: String = ""
        let converter = NotationConverter()
        for (index, simpleMove) in simpleMoves.enumerated() {
            let i = simpleMove.startField.i
            let j = simpleMove.startField.j
            string+=converter.convertCoordinateToHumanNotation(coordinate: i, toLetter: true)!
            string+=converter.convertCoordinateToHumanNotation(coordinate: j, toLetter: false)!
            if (!showBeats) {
                string+="-"
            } else {
                if simpleMove.beatField == nil {
                    string+="-"
                } else {
                    string+="x"
                }
            }
            if index == simpleMoves.count - 1 {
                let i = simpleMove.endField.i
                let j = simpleMove.endField.j
                string+=converter.convertCoordinateToHumanNotation(coordinate: i, toLetter: true)!
                string+=converter.convertCoordinateToHumanNotation(coordinate: j, toLetter: false)!
            }
        }
        return string
    }
    
    static func getStringRepresentation (for moves: [Move]) -> String {
        var string = ""
        for (index, move) in moves.enumerated() {
            let moveNumber = index % 2 == 0 ? "\(index/2+1). " : ""
            string += moveNumber
            string+=move.getStringRepresentation(showBeats: true)
            string+="\n"
        }
        return string
    }
}
