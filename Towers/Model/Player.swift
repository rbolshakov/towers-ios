//
//  File.swift
//  Towers
//
//  Created by Roman Bolshakov on 07.05.2021.
//

import Foundation

class Player {
    var game: Game
    
    init (game: Game) {
        self.game = game
    }
    
    func askForNextMove(position: Position) {
    }
    
    var name: String = "Unknown"
}

protocol AIPlayerDelegate {
    func startedThinking()
    func endedThinking()
}

class AIPlayer: Player {
    
    enum Strength: Int {
        case Very_Easy = 1
        case Easy = 2
        case Normal = 3
        case Hard = 4
        case Very_Hard = 5
        
        var timeInterval: TimeInterval {
            switch self {
            case .Very_Easy:
                return TimeInterval(0.1)
            case .Easy:
                return TimeInterval(0.3)
            case .Normal:
                return TimeInterval(1.0)
            case .Hard:
                return TimeInterval(3.0)
            case .Very_Hard:
                return TimeInterval(10.0)
            }
        }
    }
    
    var computer: AIModule!
    var delegate: AIPlayerDelegate? = nil
    var strength: Strength
    
    init(game: Game, module: AIModule, strength: Strength = .Easy) {
        self.strength = strength
        super.init(game: game)
        self.name = "Computer level \(strength)"
        self.computer = module
    }
    
    override func askForNextMove(position: Position) {
        delegate?.startedThinking()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            let (move, score) = self.computer.findBestMove(position: position, strength: self.strength)
                print("TOTAL: \(self.computer.total)")
                self.delegate?.endedThinking()
                if let score = score {
                    let correctedScore = position.whiteTurn ? score : -score
                    self.game.sendScore(score: correctedScore)
                } else {
                    self.game.sendScore(score: nil)
                }
                //DANGER
                self.game.makeMove(move:move!)
           
        }
    }
}

class HumanPlayer: Player {
    override func askForNextMove(position: Position) {
        
    }
    
    override init (game: Game) {
        super.init(game: game)
        self.name = GameSettings.defaultSettings.humanPlayerName
    }

    func makeMove (move: Move) {
        game.makeMove(move: move)
    }
}
