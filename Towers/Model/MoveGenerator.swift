//
//  Game.swift
//  Towers
//
//  Created by Roman Bolshakov on 04.05.2021.
//

import Foundation

extension MoveGenerator.Direction {
    init?(move: SimpleMove) {
        let (startI, startJ) = move.startField
        let (endI, endJ) = move.endField
        if endJ > startJ {
            if endI > startI {
                self = .right_up
            } else {
                self = .left_up
            }
        } else {
            if endI > startI {
                self = .right_down
            } else {
                self = .left_down
            }
        }
    }
}

class MoveGenerator {
    
    //HELPERS
    enum Direction {
        case left_up, right_up, left_down, right_down
        
        var singleMove: (Int, Int) {
            switch self {
            case .left_up:
                return (-1,1)
            case .right_up:
                return (1,1)
            case .left_down:
                return (-1,-1)
            case .right_down:
                return (1,-1)
            }
        }
        
        var doubleMove: (Int, Int) {
            switch self {
            case .left_up:
                return (-2,2)
            case .right_up:
                return (2,2)
            case .left_down:
                return (-2,-2)
            case .right_down:
                return (2,-2)
            }
        }
        
        var inverted: Direction {
            switch self {
            case .left_up:
                return .right_down
            case .right_up:
                return .left_down
            case .left_down:
                return .right_up
            case .right_down:
                return .left_up
            }
        }
    }
    
    let moveMaker = MoveMaker()

    
    
    
    func gameIsFinished(position: Position) -> Bool {
        let moves = getAllMoves(position: position)
        return moves.isEmpty
    }
    
    //GENERATOR OF MOVES
    func getAllMoves(position: Position) -> [Move] {
        var allQuiets: [Move] = []
        var allCaptures: [Move] = []
        for i in 0..<8 {
            for j in 0..<8 {
                if let tower = position.checkerboard[i][j], tower.whiteOnTop == position.whiteTurn {
                    if tower.isKing {
                        var captures: [Move] = []
                        getKingCaptures(byTower: (i, j), position: position, fullMove: nil, fullList: &captures)

                        allCaptures+=captures
                        if allCaptures.count == 0 {
                            let quiets = getKingMoves(byTower: (i, j), position: position)
                            allQuiets+=quiets
                        }
                    } else {
                        var captures: [Move] = []
                        getCheckerCaptures(byTower: (i, j), position: position, fullMove: nil, fullList: &captures)
                        allCaptures+=captures
                        if allCaptures.count == 0 {
                            let quiets = getCheckerMoves(byTower: (i, j), position: position)
                            allQuiets+=quiets
                        }
                    }
                }
            }
        }
        return allCaptures.count > 0 ? allCaptures : allQuiets
    }
    
    func checkerMove (isWhite: Bool, field:(i: Int,j: Int), direction: Direction, position: Position) -> Move? {
        let (hor, ver) = direction.singleMove
        let kingHor = Position.KingHor(isWhite: isWhite)
        if Position.isValidField(field.i + hor, field.j + ver) && position.checkerboard[field.i+hor][field.j+ver] == nil {
            let becameKing = (field.j + ver == kingHor)
            let move = SimpleMove(startField: (field.i, field.j), endField: (field.i + hor , field.j + ver), becameKing: becameKing, beatField: nil, beatKing: false)
            return Move(move)
        }
        return nil
    }
    
    func getCheckerMoves (byTower field:(i: Int,j: Int), position: Position) -> [Move] {
        var list: [Move] = []
        guard let tower = position.checkerboard[field.i][field.j] else {
            return list
        }
        if tower.whiteOnTop {
            if let move = checkerMove(isWhite: true, field: field, direction: Direction.left_up, position: position) {
                list+=[move]
            }
            if let move = checkerMove(isWhite: true, field: field, direction: Direction.right_up, position: position) {
                list+=[move]
            }
        } else {
            if let move = checkerMove(isWhite: false, field: field, direction: Direction.left_down, position: position) {
                list+=[move]
            }
            if let move = checkerMove(isWhite: false, field: field, direction: Direction.right_down, position: position) {
                list+=[move]
            }
        }
        
        return list
    }
    
    func kingMoves (field:(i: Int,j: Int), direction: Direction, position: Position) -> [Move] {
        var moves: [Move] = []
        let (hor, ver) = direction.singleMove
        var i = field.i
        var j = field.j
        
        while (Position.isValidField(i + hor, j + ver) && position.checkerboard[i + hor][j + ver] == nil) {
            let move = SimpleMove(startField: (field.i, field.j), endField: (i + hor, j + ver), becameKing: false, beatField: nil, beatKing: false)
            moves+=[Move(move)]
            i+=hor
            j+=ver
        }
        return moves
    }
    
    func getKingMoves(byTower field:(i: Int,j: Int), position: Position) -> [Move] {
        var list: [Move] = []
        guard position.checkerboard[field.i][field.j] != nil else {
            return list
        }
        list += kingMoves(field: field, direction: Direction.left_up, position: position)
        list += kingMoves(field: field, direction: Direction.right_up, position: position)
        list += kingMoves(field: field, direction: Direction.left_down, position: position)
        list += kingMoves(field: field, direction: Direction.right_down, position: position)
        return list
    }
    
    func checkerCaptures (field:(i: Int,j: Int), direction: Direction, position: Position, fullMove: Move?, fullList: inout [Move]) -> [SimpleMove] {
        var list: [SimpleMove] = []
        
        guard let tower = position.checkerboard[field.i][field.j] else {
            return list
        }
        
        var candidates: [Move] = []
        var canContinueCapture = false
        
        let (hor, ver) = direction.singleMove
        let (horD, verD) = direction.doubleMove
        
        let kingHor = Position.KingHor(isWhite: tower.whiteOnTop)
        if Position.isValidField(field.i + horD, field.j + verD)  {
            if let enemyTower = position.checkerboard[field.i+hor][field.j+ver], tower.whiteOnTop != enemyTower.whiteOnTop {
                if position.checkerboard[field.i + horD][field.j + verD] == nil {
                    let becameKing = (field.j + verD == kingHor)
                    let move = SimpleMove(startField: (field.i, field.j), endField: (field.i + horD, field.j + verD), becameKing: becameKing, beatField: (field.i + hor, field.j + ver), beatKing: enemyTower.isKing)
                    
                    var copiedMove: Move = fullMove ?? Move()
                    let lastBeat = copiedMove.simpleMoves.last?.beatField
                    if lastBeat == nil || lastBeat!.i != move.beatField!.i || lastBeat!.j != move.beatField!.j {
                        list+=[move]
                        
                        copiedMove.addMove(move: move)
                        let nextPosition = moveMaker.makeSimpleMove(position: position, move: move)

                        if becameKing {
                            if let candidate = getKingCaptures(byTower: move.endField, position: nextPosition, fullMove: copiedMove, fullList: &fullList) {
                                candidates+=[candidate]
                            } else {
                                canContinueCapture = true
                            }
                        } else {
                            getCheckerCaptures(byTower: move.endField, position: nextPosition, fullMove: copiedMove, fullList: &fullList)
                        }
                    }
                }
            }
        }
        
        if !canContinueCapture {
            fullList += candidates
        }
        
        return list
    }
    
    @discardableResult
    func getCheckerCaptures(byTower field:(i: Int,j: Int), position: Position, fullMove: Move?, fullList: inout [Move]) -> [Move] {
        var list: [SimpleMove] = []
        list += checkerCaptures(field:field, direction: Direction.left_up, position: position, fullMove: fullMove, fullList: &fullList )
        list += checkerCaptures(field:field, direction: Direction.right_up, position: position, fullMove: fullMove, fullList: &fullList )
        list += checkerCaptures(field:field, direction: Direction.left_down, position: position, fullMove: fullMove, fullList: &fullList )
        list += checkerCaptures(field:field, direction: Direction.right_down, position: position, fullMove: fullMove, fullList: &fullList )
        
        if list.count == 0 && fullMove != nil {
            fullList+=[fullMove!]
        }
        
        return fullList
    }
    
    func kingCaptures (field:(i: Int,j: Int), direction: Direction, position: Position, fullMove: Move?, fullList: inout [Move]) -> [SimpleMove] {
        var list: [SimpleMove] = []
        guard let tower = position.checkerboard[field.i][field.j] else {
            return list
        }
        var foundEnemy: (Int, Int)? = nil
        var foundKing = false
        var i = field.i
        var j = field.j
        let (hor, ver) = direction.singleMove
        
        
        var candidates: [Move] = []
        var canContinueCapture = false

        while Position.isValidField(i + hor, j + ver) {
            i+=hor
            j+=ver
            if let enemyTower = position.checkerboard[i][j] {
                if foundEnemy != nil || enemyTower.whiteOnTop == tower.whiteOnTop {
                    break
                } else {
                    foundEnemy = (i, j)
                    foundKing = enemyTower.isKing
                    continue
                }
            }
            if foundEnemy != nil && position.checkerboard[i][j] == nil {
                let move = SimpleMove(startField: (field.i, field.j), endField: (i, j), becameKing: false, beatField: foundEnemy, beatKing: foundKing)
                
                var copiedMove: Move = fullMove ?? Move()
                let lastBeat = copiedMove.simpleMoves.last?.beatField
                let lastMove = copiedMove.simpleMoves.last
                if lastBeat == nil || Direction(move:lastMove!)!.inverted != direction {
                    
                    list+=[move]
                    
                    
                    copiedMove.addMove(move: move)
                    let nextPosition = moveMaker.makeSimpleMove(position: position, move: move)
                    if let candidate = getKingCaptures(byTower: move.endField, position: nextPosition, fullMove: copiedMove, fullList: &fullList) {
                        candidates+=[candidate]
                    } else {
                        canContinueCapture = true
                    }
                }
            }
        }
        
        if !canContinueCapture {
            fullList += candidates
        }
        
        return list
    }
    
    @discardableResult
    func getKingCaptures (byTower field:(i: Int,j: Int), position: Position, fullMove: Move?, fullList: inout [Move]) -> Move? {
        var list: [SimpleMove] = []
        list += kingCaptures(field:field, direction: Direction.left_up, position: position, fullMove: fullMove, fullList: &fullList )
        list += kingCaptures(field:field, direction: Direction.right_up, position: position, fullMove: fullMove, fullList: &fullList )
        list += kingCaptures(field:field, direction: Direction.left_down, position: position, fullMove: fullMove, fullList: &fullList )
        list += kingCaptures(field:field, direction: Direction.right_down, position: position, fullMove: fullMove, fullList: &fullList )
        
        if list.count == 0 {
            return fullMove
        } else {
            return nil
        }
    }
    
    func checkLineIsValidMove (line: [(Int, Int)], for position: Position) -> (valid: Bool, exact: Move?) {
        let possibleMoves = getAllMoves(position: position)
        var startingMoves: [Move] = []
        var sameMove: Move? = nil
        moveCycle: for move in possibleMoves {
            let tupleArray = move.getNumericRepresentation()
            for (index, tuple) in tupleArray.enumerated() {
                if index >= line.count {
                    startingMoves+=[move]
                    continue moveCycle
                }
                let inputTuple = line[index]
                if tuple != inputTuple {
                    continue moveCycle
                }
            }
            sameMove = move
            startingMoves+=[move]
            break
        }
        let valid = !startingMoves.isEmpty
        return (valid, sameMove)
    }
    
    func getMoveListFromString (string: String, position: Position) -> [Move] {
        var moves: [Move] = []
        let moveStrings = string.components(separatedBy: "\n")
        let generator = MoveGenerator()
        let moveMaker = MoveMaker()
        let converter = NotationConverter()
        var position: Position = position
        for moveString in moveStrings {
            var convertedString: String!
            if moveString.contains(".") {
                convertedString = moveString.components(separatedBy: ". ")[1]
            } else {
                convertedString = moveString
            }
            //TODO: failable
            let numeric = converter.numericRepresentationFromString(convertedString)
            let (_, exact) = generator.checkLineIsValidMove(line: numeric, for: position)
            if let move = exact {
                position = moveMaker.makeMove(position: position, move: move)
                moves.append(move)
            }
        }
        return moves
    }
}
