//
//  Game.swift
//  Towers
//
//  Created by Roman Bolshakov on 07.05.2021.
//

import Foundation

protocol AIModule {
    init(evaluation: EvaluationFunction)
    func findBestMove(position: Position, strength: AIPlayer.Strength) -> (Move?, Int?)
    var total: Int {get}
}

protocol EvaluationFunction {
    func score(for position: Position) -> Int
}
