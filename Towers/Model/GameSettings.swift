//
//  GameSettings.swift
//  Towers
//
//  Created by Roman Bolshakov on 08.05.2021.
//

import Foundation
struct GameSettings {
    static var defaultSettings = GameSettings()
    
    var computerPlaysWhite = false
    var computerPlaysBlack = true
    var computerStrength = AIPlayer.Strength.Normal
    var preserveKings = true
    var humanPlayerName: String = "Roman"
    var date = Date()
}
