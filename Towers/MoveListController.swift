//
//  MoveListController.swift
//  Towers
//
//  Created by Roman Bolshakov on 06.05.2021.
//

import UIKit
class MoveListController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet var table: UITableView!
    var moves: [Move] = []
    var currentMoveIndex: Int?
    var didSelectMoveWithIndex: ((Int) -> Void)?
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return moves.count + 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        if indexPath.row == 0 {
            cell.textLabel?.text = "--START--"
        }
        else {
            let move = moves[indexPath.row - 1]
            let moveNumber = (indexPath.row - 1) % 2 == 0 ? "\((indexPath.row - 1)/2+1)." : "   "
            cell.textLabel?.text = "\(moveNumber) \(move.getStringRepresentation(showBeats: true))"
        }
        if let index = currentMoveIndex, index == indexPath.row {
            cell.textLabel?.textColor = .blue
        } else {
            cell.textLabel?.textColor = .black
        }
        return cell
    }
    
    @IBAction func dismissPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        didSelectMoveWithIndex?(indexPath.row-1)
    }
}
