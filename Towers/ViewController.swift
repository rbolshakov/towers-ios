//
//  ViewController.swift
//  Towers
//
//  Created by Roman Bolshakov on 03.05.2021.
//

import UIKit

class ViewController: UIViewController, GameDelegate, AIPlayerDelegate {
    @IBOutlet weak var graphicBoard: UIStackView!
    @IBOutlet weak var bottomIndicator: UIView!
    @IBOutlet weak var topIndicator: UIView!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var flipSwitch: UISwitch!
    
    let game = Game()
    var humanPlayer: HumanPlayer! = nil
    var pressedFields: [(Int, Int)] = []
    var invertedBoard = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        topIndicator.layer.cornerRadius = 6
        bottomIndicator.layer.cornerRadius = 6
        activityIndicator.isHidden = true
        game.delegate = self
        newGameAction(self)
        //testSpeed()
    }
    
    func testSpeed() {
        let start = Date()
//        for _ in 1...100000 {
//            _ = self.game.position.score
//        }
        let end = Date()
        let interval = end.timeIntervalSince(start)
        print("Interval: \(interval)")
    }
    
    @IBAction func flipBoard(_ sender: UISwitch) {
        invertedBoard = sender.isOn
        drawBoard()
    }
    
    @IBAction func saveGame(_ sender: UIButton) {
        game.saveGame { (success) in
            let message = success ? "Game is saved successfully" : "Saving game failed with error"
            let alertController = UIAlertController(title: nil,
                                                           message: message,
                                                           preferredStyle: .alert)

            let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                        alertController.addAction(defaultAction)

            present(alertController, animated: true, completion: nil)
        }
    }
    
    @IBAction func newGameAction(_ sender: Any) {
        let playerWhite = GameSettings.defaultSettings.computerPlaysWhite ? AIPlayer(game: self.game, module: AIModuleNonForced(evaluation: IntegerEvaluation()), strength: GameSettings.defaultSettings.computerStrength) : HumanPlayer(game: self.game)
        let playerBlack = GameSettings.defaultSettings.computerPlaysBlack ? AIPlayer(game: self.game, module: AIModuleNonForced(evaluation: IntegerEvaluation()), strength: GameSettings.defaultSettings.computerStrength) : HumanPlayer(game: self.game)
        if GameSettings.defaultSettings.computerPlaysWhite && !GameSettings.defaultSettings.computerPlaysBlack {
            invertedBoard = true
        } else {
            invertedBoard = false
        }
        flipSwitch!.isOn = invertedBoard
        drawBoard()
        game.newGame(players: [playerWhite, playerBlack], position: Position(.initial))
    }
    
    @IBAction func button2action (_ sender: Any) {
        undoMoveOnBoard()
    }
    
    @IBAction func button3action (_ sender: Any) {
        showMoveList()
    }
    
    @IBAction func button4action (_ sender: Any) {
        redoMoveOnBoard()
    }
    
    @IBAction func button5action (_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nav = storyBoard.instantiateViewController(withIdentifier: "setupBoardController") as! UINavigationController
        let setupBoardVC = nav.topViewController as! SetupBoardController
        setupBoardVC.position = game.position
        setupBoardVC.invertedBoard = invertedBoard
        setupBoardVC.didChangePosition = {[weak self] position in
            guard let self = self else { return }
            self.changePosition(position: position)
            self.dismiss(animated: true, completion: nil)
        }
        nav.modalPresentationStyle = .currentContext
        present(nav, animated: true, completion: nil)
    }
    
    func changePosition(position: Position) {
        pressedFields = []
        game.newGame(players: game.players, position: position)
    }
    
    
    //GRAPHIC METHODS
    func drawBoard() {
        bottomIndicator.isHidden = !game.position.whiteTurn != invertedBoard
        topIndicator.isHidden = game.position.whiteTurn != invertedBoard
        bottomIndicator.isHidden = true
        topIndicator.isHidden = true
        bottomIndicator.backgroundColor = invertedBoard ? .black : .white
        topIndicator.backgroundColor = invertedBoard ? .white : .black
        //scoreLabel.text = "\(position.score)"
        
        
        for (columnIndex, column) in graphicBoard.arrangedSubviews.enumerated() {
            for (rowIndex, towerViewplace) in (column as! UIStackView).arrangedSubviews.enumerated() {
                let towerView : TowerView = Bundle.main.loadNibNamed("TowerView", owner: self, options: nil)![0] as! TowerView
                if invertedBoard {
                    towerView.setupWithTower(tower: game.position[7-columnIndex, rowIndex])
                } else {
                    towerView.setupWithTower(tower: game.position[columnIndex, 7 - rowIndex])
                }
                towerView.frame = towerViewplace.frame
                if invertedBoard {
                    towerView.button.tag = (7-columnIndex) * 8 + 7 - rowIndex
                } else {
                    towerView.button.tag = columnIndex * 8 + rowIndex
                }
                let columnStackView = (column as! UIStackView)
                towerViewplace.removeFromSuperview()
                columnStackView.insertArrangedSubview(towerView, at: rowIndex)
            }
        }
    }
    
    func undoMoveOnBoard () {
        game.undoLastMove()
        
    }

    
    func redoMoveOnBoard () {
        game.redoLastMove()
        
    }
    
    func setFieldHighlight(i: Int, j: Int, on: Bool, color: UIColor = .red) {
        if invertedBoard {
            let verticalStack = graphicBoard.arrangedSubviews[7-i] as! UIStackView
            let towerView = verticalStack.arrangedSubviews[j] as! TowerView
            towerView.highlightedView.isHidden = !on
            towerView.highlightedView.backgroundColor = color
        } else {
            let verticalStack = graphicBoard.arrangedSubviews[i] as! UIStackView
            let towerView = verticalStack.arrangedSubviews[7-j] as! TowerView
            towerView.highlightedView.isHidden = !on
            towerView.highlightedView.backgroundColor = color
        }
    }
    
    //MAIN
    func showMoveList() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nav = storyBoard.instantiateViewController(withIdentifier: "moveListController") as! UINavigationController
        let moveListVC = nav.topViewController as! MoveListController
        moveListVC.moves = game.moves
        moveListVC.currentMoveIndex = game.currentMoveIndex
        moveListVC.didSelectMoveWithIndex = {[weak self] (moveIndex) in
            guard let self = self else { return }
            self.game.switchToMove(moveIndex: moveIndex)
            self.dismiss(animated: true, completion: nil)
        }
        nav.modalPresentationStyle = .currentContext
        present(nav, animated: true, completion: nil)
    }
    
    @IBAction func fieldPressed(_ sender: Any) {
        let tag = (sender as! UIButton).tag
        let i = tag/8
        let j = 7 - tag%8
        
        if let last = pressedFields.last, last == (i, j) {
            pressedFields.removeLast()
            setFieldHighlight(i: i, j: j, on: false)
        } else {
            pressedFields+=[(i,j)]
            setFieldHighlight(i: i, j: j, on: true)
            
            //if pressedFields.count >= 1 {
                let (valid, exact) = game.checkLineIsValidMove(line: pressedFields)
                if !valid {
                    pressedFields.removeLast()
                    setFieldHighlight(i: i, j: j, on: false)
                } else if let move = exact {
                    //pressedFields = []
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        self.pressedFields = []
                        self.humanPlayer.makeMove(move: move)
                    }
                }
            //}
        }
    }
    
    @IBAction func hintPressed(_ sender: Any) {
        startedThinking()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            let module = AIModuleForced(evaluation: IntegerEvaluation())
            let (move, _) = module.findBestMove(position: self.game.position, strength: .Hard)
//            else {
//                self.endedThinking()
//                return
//            }
            self.endedThinking()
            //DANGER
            self.humanPlayer.makeMove(move: move!)
        }
    }
    
    @IBAction func presentFilesVC() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nav = storyBoard.instantiateViewController(withIdentifier: "filesViewController") as! UINavigationController
        let filesVC = nav.topViewController as! FilesViewController
        filesVC.didLoadGame = {[weak self] (record) in
            guard let self = self else { return }
            self.game.loadGame(record: record)
            self.dismiss(animated: true, completion: nil)
            print(record.textualDescription)
        }
        nav.modalPresentationStyle = .currentContext
        present(nav, animated: true, completion: nil)
    }
    
    @IBAction func presentSettingsController() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let nav = storyBoard.instantiateViewController(withIdentifier: "settingsController") as! UINavigationController
        let vc = nav.topViewController as! SettingsController
        vc.didChangeSettings = {[weak self] in
            guard let self = self else { return }
            self.changeGameSettings()
            self.dismiss(animated: true, completion: nil)
        }
        nav.modalPresentationStyle = .currentContext
        present(nav, animated: true, completion: nil)
    }
    
    func changeGameSettings() {
        //WARNING: old players' names could be reset here
        let playerWhite = GameSettings.defaultSettings.computerPlaysWhite ? AIPlayer(game: self.game, module: AIModuleNonForced(evaluation: IntegerEvaluation()), strength: GameSettings.defaultSettings.computerStrength) : HumanPlayer(game: self.game)
        let playerBlack = GameSettings.defaultSettings.computerPlaysBlack ? AIPlayer(game: self.game, module: AIModuleNonForced(evaluation: IntegerEvaluation()), strength: GameSettings.defaultSettings.computerStrength) : HumanPlayer(game: self.game)
        self.game.players = [playerWhite, playerBlack]
        if GameSettings.defaultSettings.computerPlaysWhite && !GameSettings.defaultSettings.computerPlaysBlack {
            invertedBoard = true
        } else {
            invertedBoard = false
        }
        flipSwitch!.isOn = invertedBoard
        drawBoard()
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
//            self.game.forceMove()
//        }
    }
    
    //Game Delegate
    func gameMadeMove(move: Move) {
        //TODO: her we can try to animate move instead of simple redraw position
        drawBoard()
        //guard game.currentPlayer is AIPlayer else { return }
        let first = move.simpleMoves.first!
        let last = move.simpleMoves.last!
        //setFieldHighlight(i: first.startField.i, j: first.startField.j, on: true, color: .gray)
        setFieldHighlight(i: last.endField.i, j: last.endField.j, on: true, color: .gray)
    }
    
    func gameUndoMove(move: Move) {
        drawBoard()
    }
    
    func gameChangedTurn(player: Player) {
        if player is HumanPlayer {
            humanPlayer = player as? HumanPlayer
        } else {
            let computer = player as! AIPlayer
            computer.delegate = self
        }
    }
    
    func computerGotScore(score: Int?) {
        guard let score = score else {
            scoreLabel.text = "?"
            return
        }
        scoreLabel.text = "\(score)"
    }
    
    func gameUpdatedPosition() {
        drawBoard()
    }
    
    func gameFinished(result: Result) {
        let alertController = UIAlertController(title: nil,
                                                message: "Game is finished\n\(result.message)",
                                                       preferredStyle: .alert)

          let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                    alertController.addAction(defaultAction)

          present(alertController, animated: true, completion: nil)
    }
    
    //AI PLAYER DELEGATE
    
    func startedThinking() {
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
    }
    
    func endedThinking() {
        activityIndicator.stopAnimating()
        activityIndicator.isHidden = true
    }
    
    
    
    /*
    //TEST METHOD
    func printPossibleMoves() {
        let fullMoves = moveGenerator.getAllMoves(position: position)
        for (index, move) in fullMoves.enumerated() {
            print("#\(index + 1)")
            print(move.getStringRepresentation())
        }
    }
    
    //TEST METHOD
    func testFullMove () {
        var move = Move()
        let simple1 = SimpleMove(startField: (0,2), endField: (1,3), becameKing: false, beatField: nil, beatKing: false)
        move.addMove(move: simple1)
        let simple2 = SimpleMove(startField: (1,3), endField: (2,4), becameKing: false, beatField: nil, beatKing: false)
        move.addMove(move: simple2)
        makeMoveOnBoard(move: move)
        return
    }
    
    //TEST METHOD
    func presentMoveInput() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let testMoveController = storyBoard.instantiateViewController(withIdentifier: "testMoveController") as! TestMoveController
        testMoveController.didInputMove = {[weak self] (move, finishMove) in
            guard let self = self else { return }
            self.dismiss(animated: false, completion: { [weak self] in
                guard let self = self else { return }
                
                let possibleMoves = self.moveGenerator.getAllMoves(position: self.position)
                for possible in possibleMoves {
                    for submove in possible.simpleMoves {
                        if submove.startField == move.startField && submove.endField == move.endField {
                            self.position = self.moveMaker.makeSimpleMove(position: self.position, move: submove)
                        }
                    }
                }
                if finishMove {
                    self.position.whiteTurn = !self.position.whiteTurn
                }
                self.drawBoard()
            })
        }
        self.present(testMoveController, animated: true, completion: nil)
    } */
}

