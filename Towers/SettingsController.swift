//
//  TestMoveController.swift
//  Towers
//
//  Created by Roman Bolshakov on 04.05.2021.
//

import UIKit

class SettingsController: UIViewController {
    @IBOutlet weak var strengthLabel: UILabel!
    @IBOutlet weak var whiteSwitch: UISwitch!
    @IBOutlet weak var blackSwitch: UISwitch!
    @IBOutlet weak var kingsSwitch: UISwitch!
    @IBOutlet weak var strengthStepper: UIStepper!
    @IBOutlet weak var nameField: UITextField!
    
    var didChangeSettings: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        whiteSwitch.isOn = GameSettings.defaultSettings.computerPlaysWhite
        blackSwitch.isOn = GameSettings.defaultSettings.computerPlaysBlack
        kingsSwitch.isOn = GameSettings.defaultSettings.preserveKings
        let value = GameSettings.defaultSettings.computerStrength.rawValue
        strengthLabel.text = "\(Int(value))"
        strengthStepper.value = Double(value)
        nameField.text = GameSettings.defaultSettings.humanPlayerName
    }
    
    @IBAction func saveSettings (_ sender: Any) {
        GameSettings.defaultSettings.computerPlaysWhite = whiteSwitch.isOn
        GameSettings.defaultSettings.computerPlaysBlack = blackSwitch.isOn
        let string = strengthLabel.text!
        GameSettings.defaultSettings.computerStrength = AIPlayer.Strength(rawValue: Int(string)!)!
        GameSettings.defaultSettings.preserveKings = kingsSwitch.isOn
        if let text = nameField.text {
            GameSettings.defaultSettings.humanPlayerName = text
        }
        didChangeSettings?()
    }
    
    @IBAction func stepperClicked(_ sender: UIStepper) {
        strengthLabel.text = "\(Int(sender.value))"
    }
    
    @IBAction func dismissScreen(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
