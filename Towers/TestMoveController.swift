//
//  TestMoveController.swift
//  Towers
//
//  Created by Roman Bolshakov on 04.05.2021.
//

import UIKit

class TestMoveController: UIViewController {
    @IBOutlet weak var startIField: UITextField!
    @IBOutlet weak var startJField: UITextField!
    @IBOutlet weak var endIField: UITextField!
    @IBOutlet weak var endJField: UITextField!
    @IBOutlet weak var beatenIField: UITextField!
    @IBOutlet weak var beatenJField: UITextField!
    @IBOutlet weak var beatKingSwitch: UISwitch!
    @IBOutlet weak var becameKingSwitch: UISwitch!
    @IBOutlet weak var finishMoveSwitch: UISwitch!
    
    var didInputMove: ((SimpleMove, Bool) -> Void)?
    
    func moveFromForm() -> SimpleMove? {
        let converter = NotationConverter()
        guard let startI = converter.convertHumanNotationToCoordinate(notationSymbol: startIField.text), let startJ = converter.convertHumanNotationToCoordinate(notationSymbol: startJField.text) else { return nil }
        guard let endI = converter.convertHumanNotationToCoordinate(notationSymbol: endIField.text), let endJ = converter.convertHumanNotationToCoordinate(notationSymbol: endJField.text) else { return nil }
        
        var beatField: (Int, Int)? = nil
        if let beatI = converter.convertHumanNotationToCoordinate(notationSymbol: beatenIField.text), let beatJ = converter.convertHumanNotationToCoordinate(notationSymbol: beatenJField.text) {
            beatField = (beatI, beatJ)
        }
        
        let beatKing = beatKingSwitch.isOn
        let becameKing = becameKingSwitch.isOn
        
        let move = SimpleMove(startField: (startI, startJ), endField: (endI, endJ), becameKing: becameKing, beatField: beatField, beatKing: beatKing)
        return move
    }
    
    @IBAction func inputMove (_ sender: Any) {
        guard let move = moveFromForm() else { return }
        didInputMove?(move, finishMoveSwitch.isOn)
    }
    
    @IBAction func dismissScreen(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
