//
//  TowerView.swift
//  Towers
//
//  Created by Roman Bolshakov on 03.05.2021.
//

import UIKit

extension NSLayoutConstraint {
    /**
     Change multiplier constraint

     - parameter multiplier: CGFloat
     - returns: NSLayoutConstraint
    */
    @discardableResult
    func setMultiplier(multiplier:CGFloat) -> NSLayoutConstraint {

        NSLayoutConstraint.deactivate([self])

        let newConstraint = NSLayoutConstraint(
            item: firstItem,
            attribute: firstAttribute,
            relatedBy: relation,
            toItem: secondItem,
            attribute: secondAttribute,
            multiplier: multiplier,
            constant: constant)

        newConstraint.priority = priority
        newConstraint.shouldBeArchived = self.shouldBeArchived
        newConstraint.identifier = self.identifier

        NSLayoutConstraint.activate([newConstraint])
        return newConstraint
    }
}


class TowerView: UIView {
    @IBOutlet var stackView: UIStackView!
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var topLabel: UILabel!
    @IBOutlet var bottomLabel: UILabel!
    @IBOutlet var stackBg: UIView!
    @IBOutlet var button: UIButton!
    @IBOutlet weak var highlightedView: UIView!
    @IBOutlet weak var imageHieightConstraing: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setupWithTower(tower: Tower?) {
        stackView.layer.cornerRadius = 8
        stackBg.layer.cornerRadius = 8
        
        guard let tower = tower else {
            self.stackView.isHidden = true
            self.stackBg.isHidden = true
            self.imageView.isHidden = true
            return
        }
        
        self.imageView.isHidden = false
        
        let count = tower.white + tower.black
        let multiplier = CGFloat(1) + CGFloat(0.25) * ( CGFloat(count) - 1)
        
        stackView.isHidden = (tower.white + tower.black == 1 && !tower.isKing)
        stackView.isHidden = true
        stackBg.isHidden = stackView.isHidden
        if tower.whiteOnTop {
            topLabel.text = "\(tower.white)"
            topLabel.textColor = .white
            bottomLabel.text = "\(tower.black)"
            bottomLabel.textColor = .black
            bottomLabel.isHidden = (tower.black == 0)
        } else {
            topLabel.text = "\(tower.black)"
            topLabel.textColor = .black
            bottomLabel.text = "\(tower.white)"
            bottomLabel.textColor = .white
            bottomLabel.isHidden = (tower.white == 0)
        }
        let (image, ratio) = ImageManager().drawTower(tower: tower)
        imageView.image = image
        imageHieightConstraing.setMultiplier(multiplier:ratio)
        
        topLabel.font = tower.isKing ? UIFont.boldSystemFont(ofSize: 12) : UIFont.systemFont(ofSize: 12)
        if tower.isKing {
            topLabel.textColor = .red
        }
        bottomLabel.font = UIFont.systemFont(ofSize: 12)
    }
}
