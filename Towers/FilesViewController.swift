//
//  FilesViewController.swift
//  Towers
//
//  Created by Roman Bolshakov on 07.05.2021.
//

import Foundation

import UIKit
class FilesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet var table: UITableView!
    var urls: [URL] = []
    var didLoadGame: ((GameRecord) -> Void)?

    override func viewDidLoad() {
        urls = SaveManager().getDocuments()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return urls.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let url = urls[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = url.lastPathComponent
        return cell
    }
    
    @IBAction func dismissPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let url = urls[indexPath.row]
        guard let record = SaveManager().openFileWithUrl(fileURL: url) else {
            return
        }
        let alertController = UIAlertController(title: nil,
                                                message: record.textualDescription,
                                                       preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {[weak self] (alertAction) in
            guard let self = self else { return }
            self.didLoadGame?(record)
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(defaultAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)

    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let url = urls[indexPath.row]
            if let _ = try? FileManager.default.removeItem(at: url) {
                urls.remove(at: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: .fade)
            }
        }
    }
}
